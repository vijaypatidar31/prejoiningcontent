import java.io.*;
import java.util.Objects;
import java.util.Scanner;

public class Helper {
    public static void main(String...args) {
        Scanner scanner = new Scanner(new BufferedInputStream(System.in));
        System.out.println("Enter source folder path");
        String SOURCE_PATH = scanner.nextLine();
        System.out.println("Enter destination folder path");
        String DES_PATH = scanner.nextLine();
        System.out.println("Enter file prefix for out file");
        String PREFIX_OUT = scanner.nextLine();

        int st = 1;
        for (File f : Objects.requireNonNull(new File(SOURCE_PATH).listFiles())) {
            System.out.println(f.getName());
            File out = new File(DES_PATH, PREFIX_OUT + st+".png");
            f.renameTo(out);
            st++;
        }

    }
}
