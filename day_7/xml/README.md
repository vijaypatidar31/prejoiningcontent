# XML : Extensible Markup Language

XML is a software and hardware independent tool for storing and transporting data.

- XML stands for Extensible Markup Language
- XML is a markup language much like HTML
- XML was designed to store and transport data
- XML was designed to be self-descriptive
- XML is a W3C Recommendation

###### Example

```xml
<?xml version="1.0" encoding="UTF-8">
<users>
    <user>
        <name>Vijay Patidar<name>
        <phone>9098XXXXXXXX</phone>
        <email>vijay@example.com</email>
    <user>
    <user>
        <name>Rakesh Mukati<name>
        <phone>7008XXXXXXXX</phone>
        <email>rakesh@example.com</email>
    <user>
</users>
```

# XML Simplifies Things

- It simplifies data sharing
- It simplifies data transport
- It simplifies platform changes
- It simplifies data availability

XML is used in many aspects of web development. XML is often used to separate data from presentation.

# XML Tree Structure

XML documents are formed as element trees. An XML tree starts at a root element and branches from the root to child elements. All elements can have sub elements (child elements):

```xml
<root>
  <child>
    <subchild>.....</subchild>
  </child>
</root>
```

# XML Syntax Rules

- XML Documents Must Have a Root Element.
- The XML Prolog(The XML prolog is optional. If it exists, it must come first in the document.)

# XML Element

An XML element is everything from (including) the element's start tag to (including) the element's end tag. An element can contain:

- text
- attributes
- other elements
- or a mix of the above

# XML Attributes

XML elements can have attributes, just like HTML. Attributes are designed to contain data related to a specific element.

```xml
<person gender="female">
<!--here gender is an attribute whose value is female-->
```

# XML Elements vs. Attributes

Take a look at these two examples:

```xml
<person gender="female">
  <firstname>Anna</firstname>
  <lastname>Smith</lastname>
</person>
```

```xml
<person>
  <gender>female</gender>
  <firstname>Anna</firstname>
  <lastname>Smith</lastname>
</person>
```

In the first example, gender is an attribute. In the last example, gender is an element. Both examples provide the same information.

There are no rules about when to use attributes or when to use elements in XML.


# XML Namespaces
XML Namespaces provide a method to avoid element name conflicts.

# XML HttpRequest

The XMLHttpRequest object can be used to request data from a web server. The XMLHttpRequest object is a developers dream, because you can:

* Update a web page without reloading the page
* Request data from a server - after the page has loaded
* Receive data from a server  - after the page has loaded
* Send data to a server - in the background

example 
```js
var xhttp = new XMLHttpRequest();
xhttp.onload = function() {
    let data = xhttp.responseText;
    //handle data here
};
xhttp.open("GET", "/xml/users.xml", true);
xhttp.send();
```

# XML Parser
The XML DOM (Document Object Model) defines the properties and methods for accessing and editing XML.However, before an XML document can be accessed, it must be loaded into an XML DOM object. All modern browsers have a built-in XML parser that can convert text into an XML DOM object.

## Parsing a Text String into XML
This example parses a text string into an XML DOM object, and extracts the info from it with JavaScript:

```html
<html>
<body>

<p id="demo"></p>

<script>
var text = "<bookstore><book>" +
    "<title>Everyday Italian</title>" +
    "<author>Giada De Laurentiis</author>" +
    "<year>2005</year>" +
    "</book></bookstore>";

let parser = new DOMParser();
let xmlDoc = parser.parseFromString(text,"text/xml");

let title = xmlDoc.getElementsByTagName("title")[0].childNodes[0].nodeValue;

</script>

</body>
</html>
```

# DOM
The DOM defines a standard for accessing and manipulating documents:

* The HTML DOM defines a standard way for accessing and manipulating HTML documents. It presents an HTML document as a tree-structure.

* The XML DOM defines a standard way for accessing and manipulating XML documents. It presents an XML document as a tree-structure.


# XPath
XPath is a major element in the XSLT standard. XPath can be used to navigate through elements and attributes in an XML document
* XPath is a syntax for defining parts of an XML document
* XPath uses path expressions to navigate in XML documents
* XPath contains a library of standard functions
* XPath is a major element in XSLT and in XQuery
* XPath is a W3C recommendation

# XML and XSLT
With XSLT you can transform an XML document into HTML.

XSLT (eXtensible Stylesheet Language Transformations) is the recommended style sheet language for XML.

XSLT uses XPath to find information in an XML document.

# XQuery
XQuery is to XML what SQL is to databases.

XQuery was designed to query XML data.

```XQuery
for $x in doc("books.xml")/bookstore/book
where $x/price>30
order by $x/title
return $x/title
```

# XML DTD

An XML document with correct syntax is called "Well Formed".

An XML document validated against a DTD is both "Well Formed" and "Valid".

DTD stands for Document Type Definition.

A DTD defines the structure and the legal elements and attributes of an XML document.
example
```dtd
<!DOCTYPE note
[
<!ELEMENT note (to,from,heading,body)>
<!ELEMENT to (#PCDATA)>
<!ELEMENT from (#PCDATA)>
<!ELEMENT heading (#PCDATA)>
<!ELEMENT body (#PCDATA)>
]>
```

# XML Schema
An XML Schema describes the structure of an XML document, just like a DTD.

An XML document with correct syntax is called "Well Formed". An XML document validated against an XML Schema is both "Well Formed" and "Valid".

XML Schema is an XML-based alternative to DTD, example:

```xml
<xs:element name="note">

<xs:complexType>
  <xs:sequence>
    <xs:element name="to" type="xs:string"/>
    <xs:element name="from" type="xs:string"/>
    <xs:element name="heading" type="xs:string"/>
    <xs:element name="body" type="xs:string"/>
  </xs:sequence>
</xs:complexType>

</xs:element>
```