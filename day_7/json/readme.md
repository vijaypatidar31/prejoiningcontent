# JSON - JavaScript Object Notation

JSON objects are used for transferring data between server and client, XML serves the same purpose.However JSON objects have several advantages over XML.

Let’s have a look at the piece of a JSON data: It basically has key-value pairs.

```js
var student = {
  firstName: "Vijay",
  lastName: "Patidar",
  join_year: 2021,
  post: "ASE Intern",
};
```

# Features of JSON:

It is light-weight
It is language independent
Easy to read and write
Text based, human readable data exchange format

# Why use JSON?

- Standard Structure
  As we have seen so far that JSON objects are having a standard structure that makes developers job easy to read and write code, because they know what to expect from JSON.

- Light weight:
  When working with AJAX, it is important to load the data quickly and asynchronously without requesting the page re-load. Since JSON is light weighted, it becomes easier to get and load the requested data quickly.

- Scalable:
  JSON is language independent, which means it can work well with most of the modern programming language. Let’s say if we need to change the server side language, in that case it would be easier for us to go ahead with that change as JSON structure is same for all the languages.

# JSON vs. XML

### JSON

```json
{
  "students": [
    { "name": "John", "age": "23", "city": "Agra" },
    { "name": "Steve", "age": "28", "city": "Delhi" },
    { "name": "Peter", "age": "32", "city": "Chennai" },
    { "name": "Chaitanya", "age": "28", "city": "Bangalore" }
  ]
}
```

### XML

```xml
<students>
  <student>
    <name>John</name> <age>23</age> <city>Agra</city>
  </student>
  <student>
    <name>Steve</name> <age>28</age> <city>Delhi</city>
  </student>
  <student>
    <name>Peter</name> <age>32</age> <city>Chennai</city>
  </student>
  <student>
    <name>Chaitanya</name> <age>28</age> <city>Bangalore</city>
  </student>
</students>
```

# JSON to JavaScript object
```js
let data = "{'name':'vijay'}";

let object = JSON.parse(data);//thats it
``` 
# JavaScript object to JSON
```js
let object = {
    name:'vijay'
}

let data = JSON>stringify(object);
``` 