# Pre-Joining ASE Training Content

### [Day 1](day_1)

- [GIT](day_1)

### [Day 2](day_2)

- [GIT(continued...)](day_2/git_continue)
- [HTML](day_2/html)

### [Day 3](day_3)

- [CSS](day_3/CSS)

### [Day 4](day_4)

- [Bootstrap 4](day_4/bootstrap)

### [Day 5](day_5)

- [JavaScript](day_5)

### [Day 6](day_6)

- [jQuery](day_6)

### [Day 7](day_7)

- [XML](day_7/xml)
- [JSON](day_7/json)

### [Day 8](day_8)

- [XML AJAX](day_8/ajax)
- [HTTP](day_8/http)
- [SQL](day_8/sql)

### [Day 9](day_9)

- [MY SQL](day_9)
