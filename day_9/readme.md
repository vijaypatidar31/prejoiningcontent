# Create database

```sql
CREATE DATABASE DB_NAME;
```

# Show All database

```sql
SHOW DATABASES;
```

# Drop database

```sql
DROP DATABASE DB_NAME;
```

# Create Table

```sql
CREATE TABLE table_name (
    column1 datatype,
    column2 datatype,
    column3 datatype,
   ....
);
```

# Drop Table

```sql
DROP TABLE table_name;
```

# TRUNCATE Table(delete records only)

```sql
TRUNCATE TABLE table_name;
```

# Alter table

- Add column

```sql
ALTER TABLE table_name
ADD column_name datatype;
```

- Drop column

```sql
ALTER TABLE table_name
DROP COLUMN datatype;
```

- Modify column

```sql
ALTER TABLE table_name
MODIFY COLUMN column_name datatype;
//change column data type
```

# Create Constraints

Constraints can be specified when the table is created with the CREATE TABLE statement, or after the table is created with the ALTER TABLE statement.

```sql
CREATE TABLE table_name (
    column1 datatype constraint,
    column2 datatype constraint,
    column3 datatype constraint,
    ....
);
```

The following constraints are commonly used in SQL:

- NOT NULL - Ensures that a column cannot have a NULL value

```sql
CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255) NOT NULL,
    Age int
);
```

- UNIQUE - Ensures that all values in a column are different

```sql
CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    UNIQUE (ID)
);

//on multiple column
CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    CONSTRAINT UC_Person UNIQUE (ID,LastName)
);


//add via alter command
ALTER TABLE Persons
ADD UNIQUE (ID);
```

- PRIMARY KEY - A combination of a NOT NULL and UNIQUE. Uniquely identifies each row in a table

```sql
CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    PRIMARY KEY (ID)
);
```

- FOREIGN KEY - Prevents actions that would destroy links between tables

```sql
CREATE TABLE Orders (
    OrderID int NOT NULL,
    OrderNumber int NOT NULL,
    PersonID int,
    PRIMARY KEY (OrderID),
    FOREIGN KEY (PersonID) REFERENCES Persons(PersonID)
);

//using alter
ALTER TABLE Orders
ADD FOREIGN KEY (PersonID) REFERENCES Persons(PersonID;
```

- CHECK - Ensures that the values in a column satisfies a specific condition

```sql
CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    CHECK (Age>=18)
);
```

- DEFAULT - Sets a default value for a column if no value is specified

```sql
CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    City varchar(255) DEFAULT 'Sandnes'
);
```

- CREATE INDEX - Used to create and retrieve data from the database very quickly
```sql
CREATE INDEX index_name
ON table_name (column1, column2, ...);
```