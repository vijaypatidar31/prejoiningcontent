# HTTP : Hypertext Transfer Protocol

The Hypertext Transfer Protocol (HTTP) is an application-level protocol for distributed, collaborative, hypermedia information systems.

# Basic Features

There are three basic features that make HTTP a simple but powerful protocol:

- HTTP is connectionless
- HTTP is media independent
- HTTP is stateless

# HTTP Parameter

## Uniform Resource Identifiers

Uniform Resource Identifiers (URI) are simply formatted, case-insensitive string containing name, location, etc. to identify a resource, for example, a website, a web service, etc. A general syntax of URI used for HTTP is as follows:

```
URI = "http:" "//" host [ ":" port ] [ abs_path [ "?" query ]]
```

Here if the port is empty or not given, port 80 is assumed for HTTP and an empty abs_path is equivalent to an abs_path of "/". The characters other than those in the reserved and unsafe sets are equivalent to their ""%" HEX HEX" encoding.

## Character Sets

We use character sets to specify the character sets that the client prefers. Multiple character sets can be listed separated by commas. If a value is not specified, the default is the US-ASCII.

- US-ASCII
- ISO-8859-1
- ISO-8859-7

Content Encodings
A content encoding value indicates that an encoding algorithm has been used to encode the content before passing it over the network. Content coding are primarily used to allow a document to be compressed or otherwise usefully transformed without losing the identity.

All content-coding values are case-insensitive. HTTP/1.1 uses content-coding values in the Accept-Encoding and Content-Encoding header fields which we will see in the subsequent chapters.

## Content Encodings
A content encoding value indicates that an encoding algorithm has been used to encode the content before passing it over the network. Content coding are primarily used to allow a document to be compressed or otherwise usefully transformed without losing the identity.

All content-coding values are case-insensitive.

example 
* Accept-encoding: gzip
* Accept-encoding: compress
* Accept-encoding: deflate

## Media Types
HTTP uses Internet Media Types in the Content-Type and Accept header fields in order to provide open and extensible data typing and type negotiation. All the Media-type values are registered with the Internet Assigned Number Authority (IANA). The general syntax to specify media type is as follows:

media-type     = type "/" subtype *( ";" parameter )
The type, subtype, and parameter attribute names are case--insensitive.

## Header Fields
HTTP header fields provide required information about the request or response, or about the object sent in the message body. There are four types of HTTP message headers:

* General-header: These header fields have general applicability for both request and response messages.

* Request-header: These header fields have applicability only for request messages.

* Response-header: These header fields have applicability only for response messages.

* Entity-header: These header fields define meta information about the entity-body or, if no body is present, about the resource identified by the request.
